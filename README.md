# Plasma X

the futuristic desktop you had been dreamed of !

![](demo_video.mp4)

kde discuss forum: https://discuss.kde.org/t/plasma-x-a-glimpse-of-future/7161

notes:
1. WIP, these is just a core concept
2. feel free to take & implement this concept ahead of me, just remember my name okay :)
3. these actually meant to be a separate, integrated and predefined theme system like KODI

below is a snapshot details of plasma x

![concept](temporary.png)
